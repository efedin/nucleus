import json

import pkg_resources
import pytest
from fastapi.testclient import TestClient

from tft.nucleus.api.public import api

client = TestClient(api)


def test_version():
    """
    Test the version endpoint.
    """
    response = client.get("/v0.1/about")
    expected_app_version = pkg_resources.get_distribution("tft-api").version

    about = json.loads(response.content)

    assert response.status_code == 200
    assert about['app_version'] == expected_app_version


def test_404():
    """
    Test non-existing endpoint.
    """
    response = client.get("/v0.1/unknown-endpoint")

    assert response.status_code == 404
