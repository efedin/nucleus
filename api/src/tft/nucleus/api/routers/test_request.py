# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of test requests router
"""
from fastapi import APIRouter, Depends
from fastapi_versioning import version
from sqlalchemy.orm import Session

from .. import crud
from ..database import get_db
from ..schemes import test_request

router = APIRouter()


@router.post('/requests', response_model=test_request.RequestCreateOut)
@version(0, 1)  # type: ignore
def request_a_new_test(
    request_in: test_request.RequestCreateIn, session: Session = Depends(get_db)
) -> test_request.RequestCreateOut:
    """
    Create new test request handler
    """
    return crud.create_test_request(session, request_in)


@router.get('/requests/{request_id}', response_model=test_request.RequestGetOut)
@version(0, 1)  # type: ignore
def test_request_details(request_id: str, session: Session = Depends(get_db)) -> test_request.RequestGetOut:
    """
    Get test request handler
    """
    return crud.get_test_request(session, request_id)  # type: ignore
