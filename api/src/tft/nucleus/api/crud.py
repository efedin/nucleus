# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of Create, Read, Update, and Delete database operations.
"""
from uuid import UUID, uuid4

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from . import errors
from .database import Request, User
from .schemes import test_request


def get_test_request(session: Session, request_id: UUID) -> test_request.RequestGetOut:
    """
    Retrieve request from database and transform to response schema.
    """
    request = session.query(Request).filter(Request.id == request_id).first()
    if not request:
        raise errors.NoSuchEntityError()
    request_out = test_request.RequestGetOut(
        user_id=request.user_id,
        test=request.test,
        state=request.state,
        run=request.run,
        created=request.created,
        updated=request.updated,
    )
    return request_out


def create_test_request(
    session: Session, test_request_create_in: test_request.RequestCreateIn
) -> test_request.RequestCreateOut:
    """
    Create test request in database and transform it to response schema.
    """
    user = session.query(User).filter(User.api_key == test_request_create_in.api_key).first()
    if not user:
        raise errors.NotAuthorizedError
    request_id = str(uuid4())
    db_test_request = Request(
        id=request_id,
        user_id=user.id,
        test=jsonable_encoder(test_request_create_in.test),
        environments_requested=jsonable_encoder(test_request_create_in.environments),
        notification=jsonable_encoder(test_request_create_in.notification),
    )
    session.add(db_test_request)
    session.commit()
    session.refresh(db_test_request)
    request_out = test_request.RequestCreateOut(
        id=request_id,
        test=test_request_create_in.test,
        notification=test_request_create_in.notification,
        environments=test_request_create_in.environments,
        created=db_test_request.created,
        updated=db_test_request.updated,
    )
    return request_out
