# Testing Farm Service

Testing Farm Service is a Testing System as a Service. It provides a https://api.dev.testing-farm.io[REST API] which our users can use for running their test workloads.

## Setup

To setup python virtual environment, execute the next commands:
[shell]
....
$ cd api
$ poetry install
....

## Usage

To run the API server, execute the next commands:

[shell]
....
$ cd api
$ poetry run uvicorn tft.nucleus.api.public:api
....

## Architrecture

This repository hosts the core parts of the service:

* api - implementation of the API endpoints

## FAQ

### What is `Nucleus`

The current codename for this repository. `Nucleus` is a Latin word for the seed inside a fruit. We have chosen it to reflect the fact this repository hosts the core parts of the service.
